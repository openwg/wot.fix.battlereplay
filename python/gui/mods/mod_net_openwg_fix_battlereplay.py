import BattleReplay

BINARY_DATA_NAMES = ('damageEventList', 'xpReplay', 'creditsReplay', 'tmenXPReplay', 'goldReplay', 
       'crystalReplay', 'freeXPReplay', 'avatarDamageEventList', 'eventCoinReplay', 'bpcoinReplay', 
       'equipCoinReplay', 'battlePassPointsReplay', )

def _JSON_Encode_fixed(obj):
  if isinstance(obj, dict):
    newDict = {}
    for key, value in obj.iteritems():
      if key in BINARY_DATA_NAMES:
        value = None
      if isinstance(key, tuple):
        newDict[str(key)] = _JSON_Encode_fixed(value)
      else:
        newDict[key] = _JSON_Encode_fixed(value)

    return newDict
  if isinstance(obj, (list, tuple, set, frozenset)):
    newList = []
    for value in obj:
      newList.append(_JSON_Encode_fixed(value))

    return newList
  return obj

BattleReplay._JSON_Encode = _JSON_Encode_fixed